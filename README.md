# Apache Kafka
The repository contains manifests for the pod and service in the
Kubernetes cluster, Dockerfile instructions and CI/CD pipeline script.
To save RAM, the StatefulSet contains one replica.
For StatefulSet with 3 replicas use:
https://github.com/IBM/kraft-mode-kafka-on-kubernetes